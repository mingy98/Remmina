{
    "app-id": "org.remmina.Remmina",
    "runtime": "org.gnome.Platform",
    "runtime-version": "3.32",
    "sdk": "org.gnome.Sdk",
    "command": "remmina",
    "cleanup": [
        "*.a",
        "*.la",
        "/build-aux",
        "/include",
        "/lib/cmake",
        "/lib/pkgconfig",
        "/share/gnome-session",
        "/share/gtk-doc",
        "/share/man",
        "/share/pkgconfig",
        "/share/xsessions"
    ],
    "finish-args": [
        /* X11 + XShm access */
        "--share=ipc", "--socket=x11",
        /* Needs to talk to the network */
        "--share=network",
        /* Play sounds redirected from guests */
        "--socket=pulseaudio",
        /* Wayland access */
        "--socket=wayland",
        /* Smartcard access - Requires flatpak >= 1.3.2 (April 2019) */
        "--socket=pcsc",
        /* SSH_AUTH_SOCK access - Requires flatpak >= 0.99.1 */
        "--socket=ssh-auth",
        /* File transfer (although limited to user directory) */
        "--filesystem=home",
        /* Desktop notifications */
        "--talk-name=org.freedesktop.Notifications",
        /* Needed to save login credentials */
        "--talk-name=org.freedesktop.secrets",
        /* Appindicator */
        "--talk-name=org.kde.StatusNotifierWatcher",
        /* Query GNOME Shell version (to display systray icon if supported) */
        "--talk-name=org.gnome.Shell",
        /* Local network host discovery */
        "--system-talk-name=org.freedesktop.Avahi",
        /* Needed for dconf to work */
        "--filesystem=xdg-run/dconf", "--filesystem=~/.config/dconf:ro",
        "--talk-name=ca.desrt.dconf", "--env=DCONF_USER_CONFIG_DIR=.config/dconf"
    ],
    "modules": [
        "shared-modules/libappindicator/libappindicator-gtk3-12.10.json",
        {
            "name": "avahi",
            "cleanup": [
                "/bin",
                "/lib/avahi",
                "/share/applications/*.desktop",
                "/share/avahi"
            ],
            "config-opts": [
                "--with-distro=none",
                "--disable-gobject",
                "--disable-introspection",
                "--disable-qt3",
                "--disable-qt4",
                "--disable-gtk",
                "--disable-libdaemon",
                "--disable-python",
                "--disable-pygobject",
                "--disable-mono",
                "--disable-monodoc",
                "--disable-autoipd",
                "--disable-doxygen-doc",
                "--disable-doxygen-dot",
                "--disable-doxygen-xml",
                "--disable-doxygen-html",
                "--disable-manpages",
                "--disable-xmltoman"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://www.avahi.org/download/avahi-0.7.tar.gz",
                    "sha256": "57a99b5dfe7fdae794e3d1ee7a62973a368e91e414bd0dfa5d84434de5b14804"
                }
            ]
        },
        {
            "name": "nxproxy",
            "no-autogen": true,
            "make-args": ["PREFIX=/app"],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://code.x2go.org/releases/source/nx-libs/nx-libs-3.5.99.20-1-lite.tar.gz",
                    "sha256": "6b934a98e6425b3790ab0fa56e6e1c7047e98ffdb381415afa755375ae1cb123"
                }
            ]
        },
        {
            /**
             * For smartcard support
             */
            "name": "pcsc-lite",
            "cleanup": [
                "/bin",
                "/sbin",
                "/lib/libpcscspy.so*",
                "/share/doc"
            ],
            "config-opts": [
                "--disable-libsystemd",
                "--disable-serial",
                "--disable-usb",
                "--with-systemdsystemunitdir=no"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://pcsclite.apdu.fr/files/pcsc-lite-1.8.25.tar.bz2",
                    "sha256": "d76d79edc31cf76e782b9f697420d3defbcc91778c3c650658086a1b748e8792"
                }
            ]
        },
        {
            "name": "freerdp",
            "buildsystem": "cmake-ninja",
            "cleanup": [
                "/bin",
                "/lib/libwinpr-tools2.so*",
                "/lib/libuwac0.so*"
            ],
            "config-opts": [
                "-DCMAKE_BUILD_TYPE:STRING=Release",
                "-DCMAKE_INSTALL_LIBDIR:PATH=lib",
                "-DWITH_CLIENT:BOOL=OFF",
                "-DWITH_CUPS:BOOL=ON",
                "-DWITH_JPEG:BOOL=ON",
                "-DWITH_MANPAGES:BOOL=OFF",
                "-DWITH_OSS:BOOL=OFF",
                "-DWITH_PCSC:BOOL=ON",
                "-DWITH_PULSE:BOOL=ON",
                "-DWITH_SERVER:BOOL=OFF"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://github.com/FreeRDP/FreeRDP.git",
                    "tag": "2.0.0-rc4",
                    "commit": "e21b72c95f857817b4b32b5ef5406355c005a9e8"
                }
            ],
            "modules": [
                {
                    /**
                     * libfreerdp uses xprop to try to detect keyboard layout
                     */
                    "name": "xprop",
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://xorg.freedesktop.org/releases/individual/app/xprop-1.2.4.tar.bz2",
                            "sha256": "8c77fb096e46c60032b7e2bde9492c3ffcc18734f50b395085a5f10bfd3cf753"
                        }
                    ]
                }
            ]
        },
        {
            "name": "spice-gtk",
            "build-options": {
                "env": {
                    "PYTHONPATH": "/app/build-aux"
                }
            },
            "cleanup": [
                "/bin",
                "/share/vala"
            ],
            "config-opts": [
                "PYTHON=python3",
                "--disable-introspection",
                "--disable-vala"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "http://www.spice-space.org/download/gtk/spice-gtk-0.37.tar.bz2",
                    "sha256": "1f28b706472ad391cda79a93fd7b4c7a03e84b88fc46ddb35dddbe323c923bb7"
                }
            ],
            "modules": [
                {
                    "name": "python-pyparsing",
                    "buildsystem": "simple",
                    "build-commands": [
                        "pip3 install --target=/app/build-aux ./pyparsing-2.4.0-py2.py3-none-any.whl"
                    ],
                    "sources": [
                        {
                            "type": "file",
                            "url": "https://files.pythonhosted.org/packages/dd/d9/3ec19e966301a6e25769976999bd7bbe552016f0d32b577dc9d63d2e0c49/pyparsing-2.4.0-py2.py3-none-any.whl",
                            "sha256": "9b6323ef4ab914af344ba97510e966d64ba91055d6b9afa6b30799340e89cc03"
                        }
                    ]
                },
                {
                    "name": "python-six",
                    "buildsystem": "simple",
                    "build-commands": [
                        "pip3 install --target=/app/build-aux ./six-1.12.0-py2.py3-none-any.whl"
                    ],
                    "sources": [
                        {
                            "type": "file",
                            "url": "https://files.pythonhosted.org/packages/73/fb/00a976f728d0d1fecfe898238ce23f502a721c0ac0ecfedb80e0d88c64e9/six-1.12.0-py2.py3-none-any.whl",
                            "sha256": "3350809f0555b11f552448330d0b52d5f24c91a322ea4a15ef22629740f3761c"
                        }
                    ]
                },
                {
                    /**
                     * For smartcard support
                     */
                    "name": "libcacard",
                    "config-opts": [
                        "--enable-pcsc"
                    ],
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://www.spice-space.org/download/libcacard/libcacard-2.6.1.tar.xz",
                            "sha256": "6276c6a2bd018bf14f1b97260fff093b4a2325a9177be4fc6be7c1a9e204def0"
                        }
                    ]
                },
                {
                    "name": "lz4",
                    "subdir": "lib",
                    "no-autogen": true,
                    "make-args": ["lib"],
                    "make-install-args": ["PREFIX=/app"],
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://github.com/lz4/lz4/archive/v1.9.1/lz4-1.9.1.tar.gz",
                            "sha256": "f8377c89dad5c9f266edc0be9b73595296ecafd5bfa1000de148096c50052dc4"
                        }
                    ]
                },
                {
                    /**
                     * For webdav (shared folder) support
                     */
                    "name": "phodav",
                    "buildsystem": "meson",
                    "cleanup": [
                        "/bin",
                        "/sbin"
                    ],
                    "config-opts": [
                        "-Dgtk_doc=disabled",
                        "-Dsystemd=disabled"
                    ],
                    "sources": [
                        {
                            "type": "archive",
                            "url": "http://download.gnome.org/sources/phodav/2.3/phodav-2.3.tar.xz",
                            "sha256": "6ef61e624c30a42fb720ef7ade7447e9ba922d69024b0326dc072fa3362ebe59"
                        }
                    ]
                },
                {
                    "name": "spice-protocol",
                    "buildsystem": "meson",
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://www.spice-space.org/download/releases/spice-protocol/spice-protocol-0.14.0.tar.bz2",
                            "sha256": "b6a4aa1ca32668790b45a494bbd000e9d05797b391d5a5d4b91adf1118216eac"
                        }
                    ]
                }
            ]
        },
        {
            "name": "libsodium",
            "sources": [
                {
                    "type": "git",
                    "url": "https://github.com/jedisct1/libsodium.git",
                    "tag": "1.0.17",
                    "commit": "b732443c442239c2e0184820e9b23cca0de0828c"
                }
            ],
            "post-install": [
                "install -Dm644 -t /app/share/licenses/libsodium LICENSE"
            ]
        },
        {
            "name": "libssh",
            "buildsystem": "cmake-ninja",
            "builddir": true,
            "config-opts": [
                "-DCMAKE_BUILD_TYPE:STRING=Release",
                "-DWITH_NACL:BOOL=ON",
                "-DNACL_INCLUDE_DIR:PATH=/app/include/",
                "-DNACL_LIBRARY:PATH=/app/lib/nacl/libnacl.a",
                "-DWITH_EXAMPLES:BOOL=OFF"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://www.libssh.org/files/0.8/libssh-0.8.7.tar.xz",
                    "sha256": "43304ca22f0ba0b654e14b574a39816bc70212fdea5858a6637cc26cade3d592"
                }
            ],
            "modules": [
                {
                    "name": "nacl",
                    "buildsystem": "simple",
                    "build-commands": [
                        /* Patch build system */
                        "echo ${CC:-gcc} ${CPPFLAGS} ${CFLAGS} -fPIC >okcompilers/c",
                        "echo ${CXX:-g++} ${CPPFLAGS} ${CXXFLAGS} -fPIC >okcompilers/cpp",
                        "sed -e '/shorthostname=/ s/.*/shorthostname=flatpak-builder/g' -i ./do",
                        /* Build */
                        "./do",
                        "cat ./build/flatpak-builder/log",
                        /* Install */
                        "mkdir -v -p /app/lib/nacl",
                        "install -v -m644 -t /app/lib/nacl/ ./build/flatpak-builder/lib/*/*.a",
                        "mkdir -v -p /app/include/nacl",
                        "install -v -m644 -t /app/include/nacl/ ./build/flatpak-builder/include/*/*.h"
                    ],
                    "cleanup": [
                        "/lib/nacl"
                    ],
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://hyperelliptic.org/nacl/nacl-20110221.tar.bz2",
                            "sha256": "4f277f89735c8b0b8a6bbd043b3efb3fa1cc68a9a5da6a076507d067fc3b3bf8"
                        },
                        {
                            "type": "patch",
                            "path": "patches/nacl-20110221-cpufreq-fallback.patch"
                        }
                    ]
                }
            ]
        },
        {
            "name": "libvncserver",
            "buildsystem": "cmake-ninja",
            "builddir": true,
            "config-opts": [
                "-DCMAKE_BUILD_TYPE:STRING=Release"
            ],
            "cleanup": [
                "/bin",
                "/lib/libvncserver.so*"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://github.com/LibVNC/libvncserver/archive/LibVNCServer-0.9.12.tar.gz",
                    "sha256": "33cbbb4e15bb390f723c311b323cef4a43bcf781984f92d92adda3243a116136"
                }
            ]
        },
        {
            /**
             * Xephyr version 1.17.0 and later don't work fine with GtkSocket,
             * therefore we remain at version 1.16.4:
             *   - https://gitlab.com//Remmina//Remmina//issues//366
             *   - https://bugs.freedesktop.org//show_bug.cgi?id=91700
             */
            "name": "xephyr",
            "config-opts": [
                "--enable-kdrive",
                "--enable-kdrive-evdev",
                "--disable-kdrive-kbd",
                "--disable-kdrive-mouse",
                "--enable-xephyr",
                "--disable-xorg",
                "--disable-xvfb",
                "--disable-xnest",
                "--disable-xquartz",
                "--disable-xwayland",
                "--disable-standalone-xpbproxy",
                "--disable-xwin",
                "--disable-xfake",
                "--disable-xfbdev",
                "--disable-dmx",
                "--with-fontrootdir=/usr/share/fonts",
                "--with-xkb-bin-directory=/usr/bin",
                "--with-xkb-path=/usr/share/X11/xkb",
                "--with-xkb-output=/var/lib/xkb"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://xorg.freedesktop.org/releases/individual/xserver/xorg-server-1.16.4.tar.bz2",
                    "sha256": "abb6e1cc9213a9915a121f48576ff6739a0b8cdb3d32796f9a7743c9a6efc871"
                }
            ],
            "modules": [
                {
                    "name": "libfontenc",
                    "config-opts": [
                        "--with-fontrootdir=/usr/share/fonts"
                    ],
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://xorg.freedesktop.org/releases/individual/lib/libfontenc-1.1.4.tar.bz2",
                            "sha256": "2cfcce810ddd48f2e5dc658d28c1808e86dcf303eaff16728b9aa3dbc0092079"
                        }
                    ]
                },
                {
                    "name": "libXfont",
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://xorg.freedesktop.org/releases/individual/lib/libXfont-1.5.4.tar.bz2",
                            "sha256": "1a7f7490774c87f2052d146d1e0e64518d32e6848184a18654e8d0bb57883242"
                        }
                    ]
                }
            ]
        },
        {
            "name": "remmina",
            "buildsystem": "cmake-ninja",
            "cleanup": [
                "/bin/remmina-gnome",
                "/bin/gnome-session-remmina",
                "/share/applications/remmina-gnome.desktop"
            ],
            "config-opts": [
                "-DCMAKE_BUILD_TYPE:STRING=Release",
                "-DCMAKE_INSTALL_LIBDIR:PATH=lib",
                "-DWITH_MANPAGES:BOOL=OFF"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://gitlab.com/Remmina/Remmina.git",
                    "branch": "master"
                }
            ]
        }
    ]
}
